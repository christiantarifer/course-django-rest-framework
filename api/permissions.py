from rest_framework import permissions
# from rest_framework.permissions import (
#     BasePermission,
# )

class IsOwner(permissions.BasePermission):
    message = "No es el propietario"

    def has_object_permission(self, request, view, obj):

        # * ASK IF THE PERMISSION IS ON THE SAVE METHOD LIST (GET, OPTIONS, HEAD)
        if request.method in permissions.SAFE_METHODS:
            return True
        return request.user == obj.owner
            
