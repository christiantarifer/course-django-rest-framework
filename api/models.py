from django.conf import settings
from django.db import models

# Create your models here.

class OwnerModel(models.Model):
    owner = models.ForeignKey( settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    class Meta:
        abstract=True

class Categoria(OwnerModel):
    descripcion = models.CharField(
        max_length=100,
        help_text='Descripcion de la categoria',
        unique=True
    )

    def __str__(self):
        return f'{self.descripcion}'
    
    class Meta:
        verbose_name_plural = 'Categorías'
        verbose_name = 'Categoría'


class SubCategoria (OwnerModel):
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE)
    descripcion = models.CharField(
        max_length=100,
        help_text='Descripción de la Subcategoría'
    )
    
    
    def __str__(self):
        return f'{self.categoria.descripcion}:{self.descripcion}'


    class Meta:
        verbose_name_plural = "Sub Categorías"
        verbose_name = "Sub Categoría"
        unique_together = ('categoria', 'descripcion')


class Producto(OwnerModel):

    subcategoria = models.ForeignKey(SubCategoria, on_delete=models.CASCADE)
    descripcion = models.CharField(
        max_length=100,
        help_text='Descripción del producto',
        unique=True
    )
    fecha_creado = models.DateTimeField()
    vendido = models.BooleanField( default=True )

    def __str__(self):
        return f'{self.descripcion}'

    class Meta:
        verbose_name_plural = 'Productos'
        verbose_name = 'Producto'


