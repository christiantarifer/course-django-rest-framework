#from django.shortcuts import get_object_or_404

from django.contrib.auth import authenticate

from rest_framework import generics, status
from rest_framework.generics import (
    CreateAPIView,
    ListCreateAPIView,
    RetrieveDestroyAPIView
)
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from .models import (
    Categoria,
    Producto,
    SubCategoria,
)

from .permissions import (
    IsOwner,
)

from .serializers import (
    CategoriaSerializer,
    ProductoSerializer,
    SubCategoriaSerializer,
    UserSerializer,
)

# class ProductoList(APIView):
#     def get(self, request):
#         prod = Producto.objects.all()[:20]
#         data = ProductoSerializer(prod, many=True).data

#         return Response(data)


# class ProductoDetalle(APIView):
#     def get(self, request, pk):
#         prod = get_object_or_404(Producto, pk=pk)
#         data = ProductoSerializer(prod).data
#         return Response(data)
    
class ProductoList(ListCreateAPIView):
    queryset = Producto.objects.all()
    serializer_class = ProductoSerializer


class ProductoDetalle(RetrieveDestroyAPIView):
    queryset = Producto.objects.all()
    serializer_class = ProductoSerializer


class CategoriaSave(CreateAPIView):
    serializer_class = CategoriaSerializer


class SubCategoriaSave(CreateAPIView):
    serializer_class = SubCategoriaSerializer


class CategoriaList(ListCreateAPIView):
    queryset = Categoria.objects.all()
    serializer_class = CategoriaSerializer


# class SubCategoriaList(ListCreateAPIView):
#     queryset = SubCategoria.objects.all()
#     serializer_class = SubCategoriaSerializer


class CategoriaDetalle(RetrieveDestroyAPIView):
    queryset = Categoria.objects.all()
    serializer_class = CategoriaSerializer


class SubCategoriaList(ListCreateAPIView):
    def get_queryset(self):
        queryset = SubCategoria.objects.filter(categoria_id = self.kwargs['pk'] )
        return queryset
    serializer_class = SubCategoriaSerializer


class SubCategoriaAdd(APIView):
    def post(self, request, cat_pk):
        descripcion = request.data.get('descripcion')
        data = {'categoria': cat_pk, 'descripcion': descripcion}
        serializer = SubCategoriaSerializer(data = data)
        if serializer.is_valid():
            subcat = serializer.save()
            return Response(serializer.data, status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)


class ProdcutoViewSet(ModelViewSet):
    queryset = Producto.objects.all()
    serializer_class = ProductoSerializer
    permission_classes = ([IsAuthenticated, IsOwner])



class UserCreate(CreateAPIView):
    # * OVER WRITING AUTHENTICATION AND PERMISSION
    authentication_classes = ()
    permission_classes = ()
    serializer_class = UserSerializer


class LoginView(APIView):
    permission_classes =  ()

    def post(self, request):
        username = request.data.get('username')

        password = request.data.get('password')

        user = authenticate(username=username, password=password)
        
        if user:
            return Response({'token': user.auth_token.key})
        else:
            return Response(
                {'error':'Credenciales incorrectas'},
                status = status.HTTP_400_BAD_REQUEST
            )
        
